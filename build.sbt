import Syntax.*

val projectScalaVersion      = "2.13.14"
val crossScalaVersionsValues = Seq(projectScalaVersion, "3.3.1")

val jsonanonymizer = project
  .in(file("."))
  .settings(
    organization  := "io.gitlab.mateuszjaje",
    name          := "json-anonymizer",
    doc / sources := Seq.empty,
    // Release
    releaseTagComment        := s"Releasing ${(ThisBuild / version).value} [skip ci]",
    releaseCommitMessage     := s"Setting version to ${(ThisBuild / version).value}\n[release commit]",
    releaseNextCommitMessage := s"Setting version to ${(ThisBuild / version).value} [skip ci]",
    organization             := "io.gitlab.mateuszjaje",
    resolvers += "Typesafe Releases" at "https://repo.typesafe.com/typesafe/releases/",
    scalacOptions ++= Seq(
      "-unchecked",
      "-deprecation",
      "-encoding",
      "utf8",
      "-Xfatal-warnings",
      "-feature",
      "-language:higherKinds",
      "-language:postfixOps",
      "-language:implicitConversions",
    ) ++ {
      if (scalaVersion.value.startsWith("2.13"))
        Seq(
          "-Xsource:3",
          "-Ymacro-annotations",
          "-Ywarn-unused:imports",
        )
      else if (scalaVersion.value.startsWith("3."))
        Seq(
          "-Xmax-inlines",
          "110",
        )
      else Seq.empty
    },
    libraryDependencies ++= Seq(
      "com.github.pureconfig" %% "pureconfig-core"          % "0.17.7",
      "org.typelevel"         %% "cats-core"                % "2.10.0",
      "io.circe"              %% "circe-core"               % "0.14.6",
      "io.circe"              %% "circe-parser"             % "0.14.6",
      "com.typesafe"           % "config"                   % "1.4.3",
      "org.scalatest"         %% "scalatest-flatspec"       % "3.2.19" % Test,
      "org.scalatest"         %% "scalatest-shouldmatchers" % "3.2.19" % Test,
      "ch.qos.logback"         % "logback-classic"          % "1.4.12" % Test,
    ),
    idePackagePrefix.invisible := Some("io.gitlab.mateuszjaje.jsonanonymizer"),
    logBuffered                := false,
    doc / sources              := Seq.empty,
    Test / testOptions += Tests.Filter(suiteName => !suiteName.endsWith("ISpec")),
    licenses += ("MIT", url("http://opensource.org/licenses/MIT")),
    developers           := List(Developer("Mateusz Jaje", "Mateusz Jaje", "mateuszjaje@gmail.com", url("https://gitlab.com/mateuszjaje"))),
    homepage             := Some(url("https://gitlab.com/mateuszjaje/json-anonymizer")),
    organizationName     := "Mateusz Jaje",
    organizationHomepage := Some(url("https://gitlab.com/mateuszjaje")),
    versionScheme        := Some("semver-spec"),
    scalaVersion         := projectScalaVersion,
    crossScalaVersions   := crossScalaVersionsValues,
    publishMavenStyle    := true,
    publishTo            := sonatypeCentralPublishToBundle.value,
    releaseProcess := {
      import sbtrelease.ReleaseStateTransformations.*
      Seq[ReleaseStep](
        checkSnapshotDependencies,
        inquireVersions,
        runClean,
        runTest,
        setReleaseVersion,
        commitReleaseVersion,
        tagRelease,
        setNextVersion,
        commitNextVersion,
        pushChanges,
      )
    },
  )
