package io.gitlab.mateuszjaje.jsonanonymizer

import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class EnabledNullSanitizerTest extends AnyFlatSpec with Matchers with EitherValues {

  behavior of "EnabledNullSanitizer"

  it should "drop nested nulls" in {
    val elem = io.circe.parser
      .parse("""
        |{
        |  "parentNull": null,
        |  "elem": {
        |    "nestedNull": null
        |  },
        |  "arr": [
        |    {
        |      "nestedx1" : {
        |         "nestedNull": null
        |      }
        |    },
        |    null
        |  ]
        |}
        |""".stripMargin)
      .value
    val expected = io.circe.parser
      .parse("""
        |{
        |  "elem": {},
        |  "arr":[{"nestedx1":{}}]
        |}
        |""".stripMargin)
      .value

    EnabledNullSanitizer.process(elem).value shouldBe expected
  }
}
