package io.gitlab.mateuszjaje.jsonanonymizer

import com.typesafe.config.ConfigFactory
import org.scalatest.EitherValues
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class JsonAnonymizerConfigTest extends AnyFlatSpec with EitherValues with Matchers {

  "JsonAnonymizerConfig" should "load load mode case insensitive" in {
    JsonAnonymizerConfig
      .load(ConfigFactory.parseString("""
        |anonymization-mode = strict
        |anonymization-data {}
        |""".stripMargin))
      .value shouldBe JsonAnonymizerConfig(ConfigFactory.parseString("{}"), Strict)

    JsonAnonymizerConfig
      .load(ConfigFactory.parseString("""
        |anonymization-mode = LoosE
        |anonymization-data {}
        |""".stripMargin))
      .value shouldBe JsonAnonymizerConfig(ConfigFactory.parseString("{}"), Loose)
  }

  it should "load defaults" in {
    JsonAnonymizerConfig
      .load(ConfigFactory.parseString("""
          |anonymization-mode = Strict
          |anonymization-data {}
          |""".stripMargin))
      .value shouldBe JsonAnonymizerConfig(ConfigFactory.parseString("{}"), Strict)
  }

  it should "load null sanitization config" in {
    JsonAnonymizerConfig
      .load(ConfigFactory.parseString("""
          |anonymization-mode = Strict
          |anonymization-data {}
          |null-sanitization = enabled
          |""".stripMargin))
      .value shouldBe JsonAnonymizerConfig(ConfigFactory.parseString("{}"), Strict)
  }
}
