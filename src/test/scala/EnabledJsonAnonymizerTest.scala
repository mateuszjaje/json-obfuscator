package io.gitlab.mateuszjaje.jsonanonymizer

import JsonProcessor.autoWrap

import com.typesafe.config.ConfigFactory
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class EnabledJsonAnonymizerTest extends AnyFlatSpec with Matchers {

  behavior of "EnabledJsonAnonymizer"

  val data =
    """{"leaveUntouched":"someData", "criticalNumber": 1239089123, "sensitiveValue": "TST1231231","theLeastSensitive": "Hello world access token"}"""

  it should "anonymize properly in a strict mode" in {
    val config = JsonAnonymizerConfig(
      ConfigFactory.parseString("""
      |leaveUntouched: Keep
      |sensitiveValue: First3Letters
      |theLeastSensitive: FirstLast3Letters
      |""".stripMargin.strip()),
      Strict,
    )
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      data,
    ) shouldBe """{"leaveUntouched":"someData","criticalNumber":"[***]","sensitiveValue":"TST***","theLeastSensitive":"Hel***ken"}"""
  }
  it should "anonymize properly in a loose mode" in {
    val config = JsonAnonymizerConfig(
      ConfigFactory.parseString("""
      |criticalNumber: "*"
      |sensitiveValue: First3Letters
      |theLeastSensitive: FirstLast3Letters
      |""".stripMargin.strip()),
      Loose,
    )
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      data,
    ) shouldBe """{"leaveUntouched":"someData","criticalNumber":"[***]","sensitiveValue":"TST***","theLeastSensitive":"Hel***ken"}"""
  }

  it should "not fail for wrong config instead put error to the output, regardless of the mode" in {
    val config = JsonAnonymizerConfig(
      ConfigFactory.parseString("""
          |leaveUntouched: Keep
          |criticalNumber: "*"
          |sensitiveValue: First3Letters
          |theLeastSensitive: WrongConfig
          |""".stripMargin.strip()),
      Strict,
    )
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      data,
    ) shouldBe """{"leaveUntouched":"someData","criticalNumber":"[***]","sensitiveValue":"TST***","theLeastSensitive":"removed due to bad redactor config at theLeastSensitive: Value has to be one of (case insensitive): *,dropifempty,keep,*****,zeronumber,firstlast3letters,first3letters,drop,simpleredactor,***, got wrongconfig"}"""
    val underTestLoose = new EnabledJsonAnonymizer(config.copy(anonymizationMode = Loose))
    underTestLoose.processUnsafe(
      data,
    ) shouldBe """{"leaveUntouched":"someData","criticalNumber":"[***]","sensitiveValue":"TST***","theLeastSensitive":"removed due to bad redactor config at theLeastSensitive: Value has to be one of (case insensitive): *,dropifempty,keep,*****,zeronumber,firstlast3letters,first3letters,drop,simpleredactor,***, got wrongconfig"}"""
  }

  val nestedFieldsTestExample =
    """
      |{
      |  "personalInfo": {
      |    "favouriteColor": "red",
      |    "ssn": 213412432133,
      |    "address": "221B Baker St., London, U.K."
      |  },
      |  "creditCard": {
      |    "number": "58746985145232563",
      |    "note": "just a card",
      |    "subscriptionToken": "r3u2q90r[i93qm0[ruo;c3ru3ur930vmur9zlwrvu9013` 53rmucao;xrf,;arwaq"
      |  }
      |}
      |""".stripMargin

  val nestedFieldsTestConfig =
    """
      |personalInfo {
      |  favouriteColor = Keep
      |}
      |creditCard.number = First3Letters
      |creditCard.note = Keep
      |""".stripMargin

  it should "anonymize property nested fields" in {
    val config    = JsonAnonymizerConfig(ConfigFactory.parseString(nestedFieldsTestConfig), Strict)
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      nestedFieldsTestExample,
    ) shouldBe """{"personalInfo":{"favouriteColor":"red","ssn":"[***]","address":"[***]"},"creditCard":{"number":"587***","note":"just a card","subscriptionToken":"[***]"}}"""
  }

  val nestedFieldsTestConfig2 =
    """
      |personalInfo = Keep
      |creditCard.number = First3Letters
      |creditCard.note = Keep
      |""".stripMargin

  it should "honor Keep setting for entire root" in {
    val config    = JsonAnonymizerConfig(ConfigFactory.parseString(nestedFieldsTestConfig2), Strict)
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      nestedFieldsTestExample,
    ) shouldBe """{"personalInfo":{"favouriteColor":"red","ssn":213412432133,"address":"221B Baker St., London, U.K."},"creditCard":{"number":"587***","note":"just a card","subscriptionToken":"[***]"}}"""
  }

  it should "honor Keep setting for entire root in loose mode" in {
    val config    = JsonAnonymizerConfig(ConfigFactory.parseString(nestedFieldsTestConfig2), Loose)
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      nestedFieldsTestExample,
    ) shouldBe """{"personalInfo":{"favouriteColor":"red","ssn":213412432133,"address":"221B Baker St., London, U.K."},"creditCard":{"number":"587***","note":"just a card","subscriptionToken":"r3u2q90r[i93qm0[ruo;c3ru3ur930vmur9zlwrvu9013` 53rmucao;xrf,;arwaq"}}"""
  }

  val nullifyRootField =
    """
      |personalInfo = Keep
      |creditCard = Drop
      |""".stripMargin

  it should "nullify root field" in {
    val config    = JsonAnonymizerConfig(ConfigFactory.parseString(nullifyRootField), Strict)
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      nestedFieldsTestExample,
    ) shouldBe """{"personalInfo":{"favouriteColor":"red","ssn":213412432133,"address":"221B Baker St., London, U.K."},"creditCard":null}"""
  }

  val nullifyNestedField =
    """
      |personalInfo = Keep
      |creditCard.number = Drop
      |creditCard.note = Keep
      |""".stripMargin

  it should "nullify nested field" in {
    val config    = JsonAnonymizerConfig(ConfigFactory.parseString(nullifyNestedField), Strict)
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      nestedFieldsTestExample,
    ) shouldBe """{"personalInfo":{"favouriteColor":"red","ssn":213412432133,"address":"221B Baker St., London, U.K."},"creditCard":{"number":null,"note":"just a card","subscriptionToken":"[***]"}}"""
  }

  val dropEmptyCollectionsConfig =
    """
      |personalInfo = Keep
      |creditCard.signatures = DropIfEmpty
      |data = DropIfEmpty
      |data2 = DropIfEmpty
      |data3 = DropIfEmpty
      |data4 = DropIfEmpty
      |""".stripMargin

  val dropEmptyCollectionsExample =
    """
      |{
      |  "personalInfo": {
      |    "favouriteColor": "red"
      |  },
      |  "creditCard": [
      |    {"signatures": []},
      |    {"signatures": ["elem"]}
      |  ],
      |  "data": [],
      |  "data2": ["elem"],
      |  "data3": {},
      |  "data4": {"elem":"value"}
      |}
      |""".stripMargin

  it should "nullify empty collections" in {
    val config    = JsonAnonymizerConfig(ConfigFactory.parseString(dropEmptyCollectionsConfig), Strict)
    val underTest = new EnabledJsonAnonymizer(config)
    underTest.processUnsafe(
      dropEmptyCollectionsExample,
    ) shouldBe """{"data3":null,"data":null,"personalInfo":{"favouriteColor":"red"},"data2":["elem"],"creditCard":[{"signatures":null},{"signatures":["elem"]}],"data4":{"elem":"[***]"}}"""
  }
}
