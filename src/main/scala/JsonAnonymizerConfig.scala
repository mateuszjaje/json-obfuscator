package io.gitlab.mateuszjaje.jsonanonymizer

import com.typesafe.config.Config
import pureconfig.*
import pureconfig.ConfigReader.Result
import pureconfig.error.KeyNotFound

sealed trait AnonymizerMode extends Product
case object Strict          extends AnonymizerMode // greedy, undefined keys would be replaced with SimpleRedactor
case object Loose           extends AnonymizerMode // lazy, undefined keys would remain untouched

object AnonymizerMode {
  val entries: Map[String, AnonymizerMode] =
    List(Strict, Loose).map(x => x.getClass.getSimpleName.toLowerCase().stripSuffix("$") -> x).toMap

  implicit val pureconfigReader: ConfigReader[AnonymizerMode] =
    ConfigReader.fromString(value => entries.get(value.toLowerCase).toRight(KeyNotFound(value, entries.keySet)))

}

case class JsonAnonymizerConfig(
    anonymizationData: Config,
    anonymizationMode: AnonymizerMode,
)

object JsonAnonymizerConfig {

  implicit final val pureconfigReader: ConfigReader[JsonAnonymizerConfig] =
    ConfigReader.forProduct3("anonymization-data", "anonymization-mode", "null-sanitization")(
      (x: Config, y: AnonymizerMode, z: Option[NullSanitization]) => JsonAnonymizerConfig(x, y),
    )

  def load(cfg: ConfigSource): Result[JsonAnonymizerConfig] = cfg.load[JsonAnonymizerConfig]
  def load(cfg: Config): Result[JsonAnonymizerConfig]       = load(ConfigSource.fromConfig(cfg))
  def load(): Result[JsonAnonymizerConfig]                  = load(ConfigSource.default)

}
