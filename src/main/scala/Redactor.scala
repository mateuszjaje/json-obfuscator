package io.gitlab.mateuszjaje.jsonanonymizer

import io.circe.Json

trait Redactor {
  def redact(value: Json): Json
}

object Drop extends Redactor {
  override def redact(value: Json) = Json.Null
}

object DropIfEmpty extends Redactor {
  override def redact(value: Json) = value match {
    case array if array.asArray.exists(_.isEmpty) => Json.Null
    case obj if obj.asObject.exists(_.isEmpty)    => Json.Null
    case _                                        => value
  }

}

object Keep extends Redactor {
  override def redact(value: Json) = value
}

object Pass extends Redactor {
  override def redact(value: Json) = value
}

object SimpleRedactor extends Redactor {
  val redacted = Json.fromString("[***]")

  override def redact(value: Json) = SimpleRedactor.redacted
}

object First3Letters extends Redactor {
  override def redact(value: Json) = if (value.isString) value.mapString(x => s"${x.take(3)}***") else SimpleRedactor.redacted
}

object FirstLast3Letters extends Redactor {
  override def redact(value: Json) =
    if (value.isString) value.mapString(x => if (x.length > 9) s"${x.take(3)}***${x.takeRight(3)}" else s"${x.take(3)}***")
    else SimpleRedactor.redacted

}

class RedactorConfigError(path: String, detail: Option[String]) extends Redactor {
  override def redact(value: Json) =
    Json.fromString(s"removed due to bad redactor config at $path${detail.map(": " + _).getOrElse("")}")

}

object ZeroNumber extends Redactor {
  override def redact(value: Json) = if (value.isNumber) Json.fromFloatOrNull(0) else SimpleRedactor.redacted
}

object Redactor {
  val redactors = List(First3Letters, FirstLast3Letters, ZeroNumber, SimpleRedactor, Keep, Drop, DropIfEmpty).map { cls =>
    val clazz = cls.getClass
    clazz.getSimpleName.stripSuffix("$").toLowerCase -> cls
  }.toMap ++ Map( // some aliases
    "*****" -> SimpleRedactor,
    "***"   -> SimpleRedactor,
    "*"     -> SimpleRedactor,
  )

}
