package io.gitlab.mateuszjaje.jsonanonymizer

import io.circe.{Json, JsonObject}
import pureconfig.ConfigReader
import pureconfig.error.KeyNotFound

sealed trait NullSanitization extends Product

object NullSanitization {
  case object Enabled extends NullSanitization // enabled - drops all keys with explicit null

  case object Disabled extends NullSanitization

  val entries: Map[String, NullSanitization] =
    List(Enabled, Disabled).map(x => x.getClass.getSimpleName.toLowerCase().stripSuffix("$") -> x).toMap

  implicit val pureconfigReader: ConfigReader[NullSanitization] =
    ConfigReader.fromString(value => entries.get(value.toLowerCase).toRight(KeyNotFound(value, entries.keySet)))

}

trait NullSanitizer extends JsonProcessor

object NullSanitizer {
  val disabled: JsonAnonymizer = DisabledJsonAnonymizer

  def of(config: NullSanitization): NullSanitizer =
    if (config == NullSanitization.Enabled) EnabledNullSanitizer else DisabledNullSanitizer

}

object DisabledNullSanitizer extends NullSanitizer {
  val isEnabled: Boolean                                    = false
  def process(data: Json): Either[JsonProcessorError, Json] = Right(data)
}

object EnabledNullSanitizer extends NullSanitizer {
  val isEnabled: Boolean = true

  private def processInternal(data: Json): Json =
    data.deepDropNullValues.mapObject { obj =>
      val elements = obj.toList.map { case (k, v) =>
        k -> processInternal(v)
      }.toMap
      JsonObject.fromMap(elements)
    }

  def process(data: Json): Either[JsonProcessorError, Json] = Right(processInternal(data))
}
