package io.gitlab.mateuszjaje.jsonanonymizer

import io.circe.ParsingFailure

trait JsonProcessorError extends Throwable

case class CantParseInputError(reason: ParsingFailure, data: String)
    extends Throwable(s"Cannot parse input json: ${data.take(50)}...${data.takeRight(50)}")
    with JsonProcessorError

case class AnonymizerProcessingError(reason: String)
    extends Throwable(s"Got exception during anonymization $reason")
    with JsonProcessorError
