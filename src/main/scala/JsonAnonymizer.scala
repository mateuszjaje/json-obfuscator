package io.gitlab.mateuszjaje.jsonanonymizer

import cats.syntax.either.*
import com.typesafe.config.{ConfigObject, ConfigValueType}
import io.circe.{Json, JsonObject}

trait JsonAnonymizer extends JsonProcessor {
  def anonymize(data: Json): Either[JsonProcessorError, Json]

  override def process(data: Json): Either[JsonProcessorError, Json] = anonymize(data)
}

object JsonAnonymizer {
  val disabled: JsonAnonymizer = DisabledJsonAnonymizer

  def enabled(config: JsonAnonymizerConfig): JsonAnonymizer = new EnabledJsonAnonymizer(config)
}

object DisabledJsonAnonymizer extends JsonAnonymizer {
  val isEnabled: Boolean                                      = false
  def anonymize(data: Json): Either[JsonProcessorError, Json] = Right(data)
}

class EnabledJsonAnonymizer(config: JsonAnonymizerConfig) extends JsonAnonymizer {
  val isEnabled: Boolean = true
  def redactorFor(path: String): Redactor = {
    if (config.anonymizationData.hasPath(path)) {
      config.anonymizationData.getValue(path) match {
        case _: ConfigObject => Pass
        case another if another.valueType() == ConfigValueType.STRING =>
          val requestedValue = another.unwrapped().asInstanceOf[String].toLowerCase
          Redactor.redactors.getOrElse(
            requestedValue,
            new RedactorConfigError(
              path,
              Some(s"Value has to be one of (case insensitive): ${Redactor.redactors.keySet.mkString(",")}, got $requestedValue"),
            ),
          )
        case _ => new RedactorConfigError(path, None)
      }
    } else {
      if (config.anonymizationMode == Strict) SimpleRedactor else Pass
    }
  }

  def redactorFor(current: String, next: String): Redactor =
    if (current.isEmpty) redactorFor(next) else redactorFor(s"$current.$next")

  def anonymize(data: Json): Either[JsonProcessorError, Json] = {
    Either
      .catchNonFatal(doAnonymize(Seq.empty, data))
      .leftMap(x => AnonymizerProcessingError(x.getMessage))
  }

  private def doAnonymize(path: Seq[String], data: Json): Json = {
    val currentPath = path.mkString(".")
    data
      .mapArray(arr => arr.map(doAnonymize(path, _)).filterNot(_.isNull))
      .mapObject { obj =>
        if (currentPath.nonEmpty && redactorFor(currentPath) == Keep) {
          obj
        } else {
          val elements = obj.toList.map { case (k, v) =>
            k -> doAnonymize(path :+ k, redactorFor(currentPath, k).redact(v))
          }.toMap
          JsonObject.fromMap(elements)
        }
      }
  }

}
