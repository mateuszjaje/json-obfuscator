package io.gitlab.mateuszjaje.jsonanonymizer

import cats.syntax.either.*
import io.circe.Json

trait JsonProcessor {
  def isEnabled: Boolean
  def process(data: Json): Either[JsonProcessorError, Json]
}

object JsonProcessor {
  implicit def autoWrap(jsonProcessor: JsonProcessor): JsonProcessingSome = new JsonProcessingSome(List(jsonProcessor))
}

trait JsonProcessing {
  def processUnsafe(data: String): String = process(data).fold(_.getMessage, identity)

  def process(data: String): Either[JsonProcessorError, String]
}

class JsonProcessingSome(processors: List[JsonProcessor]) extends JsonProcessing {

  override def process(data: String): Either[JsonProcessorError, String] = {
    val parsedData = io.circe.parser
      .parse(data)
      .leftMap[JsonProcessorError](x => CantParseInputError(x, data))

    processors
      .foldLeft(parsedData) {
        case (Right(data), processor) => processor.process(data)
        case (left, _)                => left
      }
      .map(resultData => io.circe.Printer.noSpaces.print(resultData))
  }

}

object JsonProcessingNothing extends JsonProcessing {
  override def process(data: String): Either[JsonProcessorError, String] = Right(data)
}

object JsonProcessing {
  def of(processors: JsonProcessor*): JsonProcessing = {
    val enabledProcessors = processors.toList.filter(_.isEnabled)
    if (enabledProcessors.nonEmpty) new JsonProcessingSome(enabledProcessors) else JsonProcessingNothing
  }

}
