## Json anonymizer

[![Maven Central](https://maven-badges.herokuapp.com/maven-central/io.gitlab.mateuszjaje/json-anonymizer_3/badge.svg?style=plastic)](https://search.maven.org/artifact/io.gitlab.mateuszjaje/json-anonymizer_3)

```sbt
libraryDependencies += "io.gitlab.mateuszjaje" %% "json-anonymizer" % "1.+" // or replace with fixed version
```
